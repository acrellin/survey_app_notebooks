import cesium
import numpy as np
import pandas as pd
import warnings
import tarfile
import os
import time
import shutil
import joblib
from collections import defaultdict
from random import shuffle
from sklearn.model_selection import train_test_split


def parse_ts_data_return_paths(tarball_path, header_path=None):
    # header_path = 'data/survey_lcs.header.dat'

    try:
        shutil.rmtree('/tmp/noisification_ts_data')
    except FileNotFoundError:
        pass
    os.mkdir('/tmp/noisification_ts_data')

    start_time = time.time()

    ts_paths = cesium.data_management.parse_and_store_ts_data(
        data_path=tarball_path, output_dir='/tmp/noisification_ts_data',
        cleanup_archive=False, header_path=header_path, cleanup_header=False)

    print('Parsed & stored', len(ts_paths), 'light curves in',
          '{} minutes'.format(round((time.time() - start_time)/60, 2)))

    return ts_paths


asas_tarball_path = os.path.abspath(os.path.join('..', '..', 'survey_classifier_data',
                                                 'data', 'asas_training_set.tar.gz'))
asas_header_path = os.path.abspath(os.path.join('..', '..', 'survey_classifier_data',
                                                    'data', 'asas_training_set.header.dat'))

