import cesium
import numpy as np
import scipy
import pandas as pd
import warnings
import tarfile
import os
import time
import shutil
import joblib
from collections import defaultdict
from random import shuffle
from supersmoother import SuperSmoother
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from cesium.data_management import parse_and_store_ts_data
from cesium.features import GENERAL_FEATS, LOMB_SCARGLE_FEATS, CADENCE_FEATS
from cesium import featurize


# Parse and store TS data
def parse_ts_data_return_paths(tarball_path, header_path=None):
    # header_path = 'data/survey_lcs.header.dat'

    try:
        shutil.rmtree('/tmp/noisification_ts_data')
    except FileNotFoundError:
        pass
    os.mkdir('/tmp/noisification_ts_data')

    start_time = time.time()

    ts_paths = parse_and_store_ts_data(
        data_path=tarball_path, output_dir='/tmp/noisification_ts_data',
        cleanup_archive=False, header_path=header_path, cleanup_header=False)

    print('Parsed & stored', len(ts_paths), 'light curves in', 
          '{} minutes'.format(round((time.time() - start_time)/60, 2)))
    
    return ts_paths


# Generate features (just frequencies here)
def generate_features(ts_paths):
    start_time = time.time()

    featureset, labels = featurize.featurize_ts_files(
        ts_paths, features_to_use=['freq1_freq'],
        output_path=None)

    print("Featurized {} light curves in {} minutes.".format(len(ts_paths),
                                                             round((time.time() - start_time)/60., 2)))
    return featureset


def generate_survey_to_fpaths_dict(header_path, ts_data_dir):
    df = pd.read_csv(header_path)
    d = {survey: [os.path.join(ts_data_dir, '{}.npz'.format(os.path.splitext(fname)[0])) 
                  for fname in group['filename'].tolist()]
         for survey, group in df.groupby('target')}
    return d


asas_tarball_path = os.path.abspath(os.path.join('..', '..', 'survey_classifier_data', 
                                                 'data', 'asas_training_set.tar.gz'))
asas_header_path = os.path.abspath(os.path.join('..', '..', 'survey_classifier_data', 
                                                    'data', 'asas_training_set.header.dat'))
# test_tarball_path = os.path.abspath(os.path.join('..', 'survey_app', 'tests', 'data',
#                                                  'larger_asas_training_subset.tar.gz'))
# test_header_path = os.path.abspath(os.path.join(
#     '..', 'survey_app', 'tests', 'data',
#     'larger_asas_training_subset_classes_with_metadata.dat'))
surveys_header_path = os.path.abspath(os.path.join('..', '..', 'survey_classifier_data', 
                                                   'data', 'survey_lcs.header.dat'))
surveys_ts_data_dir = os.path.abspath(os.path.join('..', '..', 'survey_classifier', 
                                                   'data_no_dups'))

asas_fname_to_class_df = pd.read_csv(asas_header_path, squeeze=False, dtype={'filename': str})
asas_fname_to_class_df = asas_fname_to_class_df.set_index('filename')


asas_ts_paths = parse_ts_data_return_paths(tarball_path=asas_tarball_path, 
                                           header_path=asas_header_path)

asas_featureset, data = featurize.load_featureset('asas_freqs_featureset.npz')

survey_to_fpaths_dict = generate_survey_to_fpaths_dict(surveys_header_path, 
                                                       surveys_ts_data_dir)

print(asas_featureset.loc['215707'].freq1_freq.values[0])

import glob

try:
    survey_to_noisified_fpaths
except NameError:
    survey_to_noisified_fpaths = {}

for survey in survey_to_fpaths_dict:
    if survey not in survey_to_noisified_fpaths:
        survey_to_noisified_fpaths[survey] = glob.glob('data_no_dups_many2/noisified_{}_lcs/*.npz'.format(survey))

del survey_to_noisified_fpaths['ASAS']
del survey_to_noisified_fpaths['LINEAR']

for k in survey_to_noisified_fpaths:
    print(k, len(survey_to_noisified_fpaths[k]))


all_fpaths = []
all_labels = []
for survey in survey_to_noisified_fpaths:
    all_fpaths += survey_to_noisified_fpaths[survey]
    all_labels += [survey] * len(survey_to_noisified_fpaths[survey])

print(len(all_fpaths), len(all_labels))    

nsfd_survey_fset, _ = featurize.featurize_ts_files(all_fpaths, CADENCE_FEATS)

featurize.save_featureset(nsfd_survey_fset, 'nsfd_fset_7surveys.npz', labels=all_labels)

print(' * ' * 20, 'Done.')
