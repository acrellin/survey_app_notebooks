catalina_to_asas_var_type = {
    'ACep': None, 'Blazkho': None, 'Cep': 'Multiple', 'Delta_Scuti': 'Delta_Scuti', 'EA': 'Beta_Persei', 'Eclipsing_Binary': 'W_Ursae_Maj',
    'LMC Classical Cep': 'Classical_Cepheid', 'LPV': None, 'Misc': None, 'RRab': 'RR_Lyrae_FM', 'RRc': 'RR_Lyrae_FO', 'RRd': 'RR_Lyrae_DM',
    'Rotational': 'Delta_Scuti'}
